import os
import logging

import torch
import torch.nn as nn
from einops import rearrange, repeat

import model_cfgs
import faster_simdr as simdr


def export_dec3():
    inp = torch.randn(1, 3, 256, 256)  # Seems to be correct - 1, 2, 64, 64 don't work, neither does 1, 3, 128, 128 for example
    net = simdr.get_pose_net(model_cfgs.config_moses_w16_31mb, try_restart = True)

    print("Big - num parameters:", sum(p.numel() for p in net.parameters() if p.requires_grad))


    torch.onnx.export(net,         # model being run
                    inp,       # model input (or a tuple for multiple inputs)
                    "trained-simdr-w16.onnx",       # where to save the model
                    export_params=True,  # store the trained parameter weights inside the model file
                    opset_version=13,    # the ONNX version to export the model to
                    do_constant_folding=True,  # whether to execute constant folding for optimization
                    input_names=['inputImg'],   # the model's input names
                    output_names=['x_axis_hmap', 'y_axis_hmap'],  # the model's output names
                    # export_raw_ir=True,

                    #   dynamic_axes={'inputImg': {0: 'batch_size'},    # variable length axes
                    #                 'x_axis_hmap': {0: 'batch_size'},
                    #                 'y_axis_hmap': {0: 'batch_size'}}
                    )
    print(" ")
    print('Model has been converted to ONNX')


def export_tiny():
    inp = torch.randn(1, 3, 256, 256, dtype=torch.float32)  # Seems to be correct - 1, 2, 64, 64 don't work, neither does 1, 3, 128, 128 for example
    net = simdr.get_pose_net(model_cfgs.config_moses_tiny, try_restart = False)

    print("Small - num parameters:", sum(p.numel() for p in net.parameters() if p.requires_grad))


    torch.onnx.export(net,         # model being run
                    inp,       # model input (or a tuple for multiple inputs)
                    "test-small.onnx",       # where to save the model
                    export_params=True,  # store the trained parameter weights inside the model file
                    opset_version=13,    # the ONNX version to export the model to
                    do_constant_folding=True,  # whether to execute constant folding for optimization
                    input_names=['inputImg'],   # the model's input names
                    output_names=['x_axis_hmap', 'y_axis_hmap'],  # the model's output names
                    # export_raw_ir=True,

                    #   dynamic_axes={'inputImg': {0: 'batch_size'},    # variable length axes
                    #                 'x_axis_hmap': {0: 'batch_size'},
                    #                 'y_axis_hmap': {0: 'batch_size'}}
                    )
    print(" ")
    print('Model has been converted to ONNX')\


def export_hmap():
    inp = torch.randn(1, 3, 256, 256, dtype=torch.float32)  # Seems to be correct - 1, 2, 64, 64 don't work, neither does 1, 3, 128, 128 for example
    net = simdr.get_pose_net(model_cfgs.config_moses_hmap, try_restart = False)

    print("Small - num parameters:", sum(p.numel() for p in net.parameters() if p.requires_grad))


    torch.onnx.export(net,         # model being run
                    inp,       # model input (or a tuple for multiple inputs)
                    "test-hmap.onnx",       # where to save the model
                    export_params=True,  # store the trained parameter weights inside the model file
                    opset_version=13,    # the ONNX version to export the model to
                    do_constant_folding=True,  # whether to execute constant folding for optimization
                    input_names=['inputImg'],   # the model's input names
                    output_names=['hmap'],  # the model's output names
                    verbose=True,


                    #   dynamic_axes={'inputImg': {0: 'batch_size'},    # variable length axes
                    #                 'x_axis_hmap': {0: 'batch_size'},
                    #                 'y_axis_hmap': {0: 'batch_size'}}
                    )
    print(" ")
    print('Model has been converted to ONNX')

def export_dec5():
    inp = torch.randn(1, 3, 224, 224, dtype=torch.float32)  # Seems to be correct - 1, 2, 64, 64 don't work, neither does 1, 3, 128, 128 for example
    net = simdr.get_pose_net(model_cfgs.config_moses_dec5_2, try_restart = False)

    print("Small - num parameters:", sum(p.numel() for p in net.parameters() if p.requires_grad))


    torch.onnx.export(net,         # model being run
                    inp,       # model input (or a tuple for multiple inputs)
                    "test-faster.onnx",       # where to save the model
                    export_params=True,  # store the trained parameter weights inside the model file
                    opset_version=13,    # the ONNX version to export the model to
                    do_constant_folding=True,  # whether to execute constant folding for optimization
                    input_names=['inputImg'],   # the model's input names
                    output_names=['hmap'],  # the model's output names
                    verbose=True,


                    #   dynamic_axes={'inputImg': {0: 'batch_size'},    # variable length axes
                    #                 'x_axis_hmap': {0: 'batch_size'},
                    #                 'y_axis_hmap': {0: 'batch_size'}}
                    )
    print(" ")
    print('Model has been converted to ONNX')

if __name__ == "__main__":

    # export_dec3()
    # export_tiny()
    # export_hmap()
    export_dec5()

import onnxruntime as rt
import numpy as np
import time

def main() -> None:
    path = "trained-simdr-w16.onnx"
    path = "test-small.onnx"
    path = "test-hmap.onnx"
    path = "/home/moses/.local/share/monado/hand-tracking-models/hand_landmark_MEDIAPIPE.onnx"

    sess_options = rt.SessionOptions()
    # sess_options.intra_op_num_threads = 1
    sess_options.graph_optimization_level = rt.GraphOptimizationLevel.ORT_ENABLE_ALL
    # sess_options.execution_mode = rt.ExecutionMode.

    sess = rt.InferenceSession(path, sess_options)
    input_name = sess.get_inputs()[0].name
    output_name_0 = sess.get_outputs()[0].name
    # output_name_1 = sess.get_outputs()[1].name

    input_shape = sess.get_inputs()[0].shape

    batch_size = 1
    dummy_input = np.random.random((batch_size, *input_shape[1:])).astype(np.float32)
    for i in range(200):
      start = time.time()
      prediction = sess.run(None, {input_name: dummy_input})[0]
      end = time.time()
      print(end-start)



if __name__ == "__main__":

    main()

# 64x64 input, 64x64 heatmap: head_input = 256
# 128x128 input, 128x128 heatmap: head_input = 1024
# 256x256 input, 256x256 heatmap: head_input = 4096

input_side = 256

# Huge - W48
config_w48_huge = {
    "MODEL": {
        "IN_PLANES": 64,
        "SIMDR_SPLIT_RATIO": 2.0,
        "HEAD_INPUT": int((input_side**2)/16),
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4],
                "NUM_CHANNELS": [48, 96],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 4,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4],
                "NUM_CHANNELS": [48, 96, 192],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 3,
                "NUM_BRANCHES": 4,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4, 4],
                "NUM_CHANNELS": [48, 96, 192, 384],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

# Also huge but less huge - W32
config_w32_huge = {
    "MODEL": {
        "IN_PLANES": 64,
        "SIMDR_SPLIT_RATIO": 2.0,
        "HEAD_INPUT": int((input_side**2)/16),
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4],
                "NUM_CHANNELS": [32, 64],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 4,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4],
                "NUM_CHANNELS": [32, 64, 128],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 3,
                "NUM_BRANCHES": 4,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4, 4],
                "NUM_CHANNELS": [32, 64, 128, 256],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

# 30MB model - seems good.
config_moses_w32_30mb = {
    "MODEL": {
        "IN_PLANES": 64,
        "SIMDR_SPLIT_RATIO": 2.0,
        "HEAD_INPUT": int((input_side**2)/16),
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [2, 2],
                "NUM_CHANNELS": [32, 64],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [3, 3, 3],
                "NUM_CHANNELS": [32, 64, 128],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4],
                "NUM_CHANNELS": [32, 64, 128],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

input_side = 256
config_moses_w16_31mb = {
    "MODEL": {
        "IN_PLANES": 64,
        "SIMDR_SPLIT_RATIO": 2.0,
        "HEAD_INPUT": int((input_side**2)/16),
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [2, 2],
                "NUM_CHANNELS": [16, 32],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [3, 3, 3],
                "NUM_CHANNELS": [16, 32, 64],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [4, 4, 4],
                "NUM_CHANNELS": [16, 32, 64],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

small: int = 8
medium: int = 16 #16
big: int = 32#16 #32

input_side = 256


config_moses_dec5 = {
    "MODEL": {
        "IN_PLANES": 24,
        # "TRANSITION_LAYER_SIZE": 96,
        "SIMDR_SPLIT_RATIO": 1.0,
        "HEAD_INPUT": 4096,
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [1, 1],
                "NUM_CHANNELS": [small, medium],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [1, 1],
                "NUM_CHANNELS": [small, medium],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [2, 2, 2],
                "NUM_CHANNELS": [small, medium, big],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

small: int = 8
medium: int = 16 #16
big: int = 32#16 #32
huge: int = 64#16 #32

input_side = 224

config_moses_dec5_2 = {
    "MODEL": {
        "IN_PLANES": 22,
        "SIMDR_SPLIT_RATIO": 1.0,
        "HEAD_INPUT": int((input_side**2)/16),
        "NUM_JOINTS": 21,
        "COORD_REPRESENTATION": "simdr",
        "IMAGE_SIZE": (input_side, input_side),
        "EXTRA": {
            "FINAL_CONV_KERNEL": 1,
            "STAGE2": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 2,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [1, 1],
                "NUM_CHANNELS": [small, medium],
                "FUSE_METHOD": "SUM"
            },
            "STAGE3": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 3,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [1, 1, 1],
                "NUM_CHANNELS": [small, medium, big],
                "FUSE_METHOD": "SUM",
            },
            "STAGE4": {
                "NUM_MODULES": 1,
                "NUM_BRANCHES": 4,
                "BLOCK": "BASIC",
                "NUM_BLOCKS": [2, 2, 2, 2],
                "NUM_CHANNELS": [small, medium, big, huge],
                "FUSE_METHOD": "SUM"
            }
        }
    }
}

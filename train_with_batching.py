import argparse
import os
import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt
from torch.utils import data
from torch.utils.data import Dataset, DataLoader
from opencv_transforms import transforms

import heatmap_1d
import faster_simdr as simdr
import model_cfgs

import scipy.optimize
import matplotlib.pyplot as plt
import time
import random

# https://gitanswer.com/pytorch-too-many-open-files-error-cplusplus-356516297
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

parser = argparse.ArgumentParser(description='Train keypoints network')
parser.add_argument('--no-gui',
                    help="Don't run the little debug gui",
                    dest='gui', action='store_false'
                    )
parser.set_defaults(gui=True)
args = parser.parse_args()

print(args.gui)

class HeatmapDataset_Train(Dataset):

    base_path = "augmentation5"

    data_sources = [
        {"filename": f"{base_path}/nikitha.csv", "proportion": 0.5},
        {"filename": f"{base_path}/frei_gs.csv", "proportion": 0.5},
        {"filename": f"{base_path}/mt_oct27.csv", "proportion": 0.3},
        {"filename": f"{base_path}/tom_17.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_23.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_24.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_26.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_27.csv", "proportion": 0.2},
        {"filename": f"{base_path}/pano_human.csv", "proportion": 0.6},
        {"filename": f"{base_path}/pano_synth.csv", "proportion": 0.8},
        {"filename": f"{base_path}/pano_pano.csv", "proportion": 1.0},
    ]

    def __init__(self, cfg):
        assert cfg["MODEL"]["IMAGE_SIZE"][0] == cfg["MODEL"]["IMAGE_SIZE"][1], "You can turn this off if you want, but currently a lot of code assumes square input size"
        self.input_size = cfg["MODEL"]["IMAGE_SIZE"][0]
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # This is what ImageNet uses?

        sum = 0.0

        for thing in self.data_sources:
            thing["csv_frame"] = pd.read_csv(thing["filename"], delimiter=" ", quotechar="|")
            thing["length"] = len(thing["csv_frame"])
            sum += thing["proportion"]
        for thing in self.data_sources:
            thing["proportion"] *= (1.0/sum)
            print(thing["proportion"])

    def __len__(self):
        c: int = 0
        for thing in self.data_sources:
            c += len(thing["csv_frame"])
        return c

    def __getitem__(self, idx):

        dataset_choice_a = random.uniform(0, 1)
        curr = 0
        thing = None
        for thinga in self.data_sources:
            curr += thinga["proportion"]
            # print("asdf", dataset_choice_a, curr, thinga["filename"])
            if dataset_choice_a < curr:
                thing = thinga
                break
        if thing == None:
            raise RuntimeError

        # print(dataset_choice_a, curr, thing["filename"], "\n\\n\n\n\n")

        # if dataset_choice_a > 0.5:
        #     thing = self.data_sources[1]

        idx = random.randint(0, thing["length"]-1)

        row = thing["csv_frame"].iloc[idx]

        # Input
        img_name = os.path.join(self.base_path,
                                row[0])  # thing["csv_frame"].iloc[idx, 0])
        image_numpy = cv2.imread(img_name)

        image_numpy = image_numpy.astype(np.float32)*1.0/255.0

        simDR_axis_size = self.input_size

        image_numpy = cv2.resize(image_numpy, (simDR_axis_size, simDR_axis_size))

        # cv2.imshow("input", image_numpy)

        for_normalize = np.zeros((3, simDR_axis_size, simDR_axis_size), dtype=np.float32)

        # Convert from interleaved to planar as well as BGR to RGB
        for_normalize[0] = image_numpy[:, :, 2]
        for_normalize[1] = image_numpy[:, :, 1]
        for_normalize[2] = image_numpy[:, :, 0]

        input_as_tensor = torch.from_numpy(for_normalize)

        input_as_tensor = self.normalize(input_as_tensor)

        for_real_this_time = np.zeros((1, 3, simDR_axis_size, simDR_axis_size), dtype=np.float32)

        for_real_this_time[0] = input_as_tensor.numpy()

        # Output
        # outputX, outputY should be 1, 21, 512 (NOT 1x1x21x512, 1x21x512).
        output_x_axis = np.zeros((1, 21, simDR_axis_size))
        output_y_axis = np.zeros((1, 21, simDR_axis_size))

        # blorg = np.zeros((512, 512))

        jointlocs = np.zeros((21, 2))

        for i in range(21):
            # y = self.landmarks_frame.iloc[idx, i*2+2] * simDR_axis_size/224
            # x = self.landmarks_frame.iloc[idx, i*2+1] * simDR_axis_size/224
            y = row[i*2+2] * simDR_axis_size/224
            x = row[i*2+1] * simDR_axis_size/224
            output_x_axis[0][i] = heatmap_1d.heatmap_1d(simDR_axis_size, x, simDR_axis_size*0.04)
            output_y_axis[0][i] = heatmap_1d.heatmap_1d(simDR_axis_size, y, simDR_axis_size*0.04)
            jointlocs[i][0] = x
            jointlocs[i][1] = y
            # blorg += heatmap_1d.two_heatmaps_to_2d(output_x_axis[0][i], output_y_axis[0][i])

        # cv2.imshow("blorg", blorg)
        # cv2.imshow("x", output_x_axis[0])
        # cv2.imshow("y", output_y_axis[0].T)

        # cv2.waitKey(0)

        sample = {
            'input_image': for_real_this_time,
            'output_x_axis': torch.from_numpy(output_x_axis).float(),
            'output_y_axis': torch.from_numpy(output_y_axis).float(),
            'disp_image': image_numpy, "gt_joint_locs": jointlocs}

        return sample


class MosesHeatmapDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, cfg, filename, test):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        assert cfg["MODEL"]["IMAGE_SIZE"][0] == cfg["MODEL"]["IMAGE_SIZE"][1], "You can turn this off if you want, but currently a lot of code assumes square input size"
        self.input_size = cfg["MODEL"]["IMAGE_SIZE"][0]
        self.landmarks_frame = pd.read_csv(filename, delimiter=" ", quotechar="|")
        self.test = test
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # This is what ImageNet uses?

    def __len__(self):
        return len(self.landmarks_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        # Input
        img_name = os.path.join("augmentation3",
                                self.landmarks_frame.iloc[idx, 0])
        image_numpy = cv2.imread(img_name)

        image_numpy = image_numpy.astype(np.float32)*1.0/255.0

        simDR_axis_size = self.input_size

        image_numpy = cv2.resize(image_numpy, (simDR_axis_size, simDR_axis_size))

        # cv2.imshow("input", image_numpy)

        for_normalize = np.zeros((3, simDR_axis_size, simDR_axis_size), dtype=np.float32)

        # Convert from interleaved to planar as well as BGR to RGB
        for_normalize[0] = image_numpy[:, :, 2]
        for_normalize[1] = image_numpy[:, :, 1]
        for_normalize[2] = image_numpy[:, :, 0]

        input_as_tensor = torch.from_numpy(for_normalize)

        input_as_tensor = self.normalize(input_as_tensor)

        for_real_this_time = np.zeros((1, 3, simDR_axis_size, simDR_axis_size), dtype=np.float32)

        for_real_this_time[0] = input_as_tensor.numpy()

        # Output
        # outputX, outputY should be 1, 21, 512 (NOT 1x1x21x512, 1x21x512).
        output_x_axis = np.zeros((1, 21, simDR_axis_size))
        output_y_axis = np.zeros((1, 21, simDR_axis_size))

        # blorg = np.zeros((512, 512))

        jointlocs = np.zeros((21, 2))

        for i in range(21):
            y = self.landmarks_frame.iloc[idx, i*2+2] * simDR_axis_size/224
            x = self.landmarks_frame.iloc[idx, i*2+1] * simDR_axis_size/224
            output_x_axis[0][i] = heatmap_1d.heatmap_1d(simDR_axis_size, x, simDR_axis_size/20)
            output_y_axis[0][i] = heatmap_1d.heatmap_1d(simDR_axis_size, y, simDR_axis_size/20)
            jointlocs[i][0] = x
            jointlocs[i][1] = y
            # blorg += heatmap_1d.two_heatmaps_to_2d(output_x_axis[0][i], output_y_axis[0][i])

        # cv2.imshow("blorg", blorg)
        # cv2.imshow("x", output_x_axis[0])
        # cv2.imshow("y", output_y_axis[0].T)

        # cv2.waitKey(0)

        sample = {
            'input_image': for_real_this_time,
            'output_x_axis': torch.from_numpy(output_x_axis).float(),
            'output_y_axis': torch.from_numpy(output_y_axis).float(),
            'disp_image': image_numpy, "gt_joint_locs": jointlocs}

        return sample


class MosesLoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.float()
        self.criterion = nn.MSELoss()

    def forward(self, pred_x, pred_y, gt_x, gt_y):
        loss = 0
        loss += self.criterion(pred_x, gt_x).sum()
        loss += self.criterion(pred_y, gt_y).sum()
        return loss


def gaus(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))


def train_loop(mini_epoch_size, mini_epoch, epoch, scheduler, dataloader, model, loss_fn, train=True, optimizer=None):
    for batch, doct in enumerate(dataloader):
        print(len(doct["input_image"]))
        input = doct["input_image"]
        gt_x = doct["output_x_axis"]
        gt_y = doct["output_y_axis"]
        print(model)

        pred = model(input)
        loss = loss_fn(pred[0], pred[1], gt_x, gt_y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if (batch % mini_epoch_size == 0):

            mini_epoch += 1
            # save
            final_output_dir = "checkpoints"
            best_model = False

            save_checkpoint({
                'mini_epoch': mini_epoch,
                'epoch': epoch,
                'model': "i dont know what to call this",
                'state_dict': model.state_dict(),
                # 'best_state_dict': model.module.state_dict(),
                'perf': 0.0,  # float
                'optimizer': optimizer.state_dict(),
            }, best_model, final_output_dir)
            print("Beginning mini-epoch", mini_epoch)
            scheduler.step()

        if batch % 10 == 0:
            # loss, current = loss.item(), batch*len(X)
            # torch.save(model.state_dict(), model.state_path)
            print(f"loss: {loss:>7f}")
            # print("PARAMETERS:", sum(p.numel() for p in model.parameters() if p.requires_grad))

            # cv2.imshow("ground-truth output:", heatmap_1d.two_heatmaps_to_2d(gt_x.detach().numpy()[0][0], gt_y.detach().numpy()[0][0]))
            # cv2.imshow("model output:", heatmap_1d.two_heatmaps_to_2d(pred[0].detach().numpy()[0][0], pred[1].detach().numpy()[0][0]))
            # cv2.imshow("model output x:", pred[0].detach().numpy()[0])
            # cv2.imshow("model output y:", pred[1].detach().numpy()[0].T)

            gt_x_np = gt_x.detach().numpy()[0]
            gt_y_np = gt_y.detach().numpy()[0]


            output_x = pred[0].detach().numpy()[0]
            output_y = pred[1].detach().numpy()[0]



            output_grid = np.zeros((224, 224, 3))
            inp_to_gauss = np.arange(224)

            jointlocs_gauss = np.zeros((21, 2))
            jointlocs_argmax = np.zeros((21, 2))

            gt_image = doct["disp_image"][0].numpy()

            for i in range(21):
                center_x = np.argmax(output_x[i])
                jointlocs_argmax[i][0] = center_x

                try:
                    popt, pcov = scipy.optimize.curve_fit(gaus, inp_to_gauss, output_x[i], p0=[.87, center_x, 13])
                    jointlocs_gauss[i][0] = popt[1]
                except RuntimeError:
                    print("Couldn't fit X!")
                    pass

                center_y = np.argmax(output_y[i])
                jointlocs_argmax[i][1] = center_y

                try:
                    popt, pcov = scipy.optimize.curve_fit(gaus, inp_to_gauss, output_y[i], p0=[.87, center_y, 13])
                    jointlocs_gauss[i][1] = popt[1]
                except RuntimeError:
                    print("Couldn't fit Y!")
                    pass

            def draw_lines(thing, color, width=2):
                for finger in range(5):
                    for joint in range(4):
                        idx = 1 + (finger*4) + joint

                        previdx = idx-1
                        if joint == 0:
                            previdx = 0

                        prev_gt = thing[previdx]
                        curr_gt = thing[idx]

                        cv2.line(gt_image, (int(prev_gt[0]), int(prev_gt[1])), (int(curr_gt[0]), int(curr_gt[1])), color, width)

            draw_lines(doct["gt_joint_locs"][0], (0, 255, 0))
            # draw_lines(jointlocs_argmax, (0, 128, 200))
            draw_lines(jointlocs_gauss, (0, 0, 255))

            # cv2.circle(output_grid, (int(cX), int(cY)), 7, (255, 255, 255), cv2.FILLED)
            # paramsX, covX = scipy.optimize.curve_fit(Gauss, inp_to_gauss, output_x[i])
            # paramsY, covY = scipy.optimize.curve_fit(Gauss, inp_to_gauss, output_y[i])

            # print(paramsX, paramsY)

            # xloc = paramsX[0]
            # yloc = paramsY[0]
            if  args.gui:
                cv2.imshow("ground-truth output:", heatmap_1d.two_heatmaps_to_2d(gt_x_np[12], gt_y_np[12]))
                cv2.imshow("model output x:", output_x)
                cv2.imshow("model output y:", output_y.T)
                cv2.imshow("model output:", heatmap_1d.two_heatmaps_to_2d(output_x[12], output_y[12]))
                cv2.imshow("preedicted joints", gt_image)

                # cv2.imshow("test! output", np.hstack((output.detach().numpy()[0][0], pred.detach().numpy()[0][0])))
                key = cv2.waitKey(1)
                # Exiting cleanly is important; sometimes you can accidentally ctrl+c when saving the state dict
                if (key == ord('q')):
                    # if (train):
                    #     print("Saving!")
                    #     torch.save(model.state_dict(), model.state_path)
                    exit(0)
            # cv2.waitKey(1)
        # if batch % 500 == 0:
        #     if (train):
        #         print("Saving!")
        #         torch.save(model.state_dict(), model.state_path)
    return mini_epoch


def save_checkpoint(states, is_best, output_dir, filename='checkpoint.pth'):
    torch.save(states, os.path.join(output_dir, filename))
    if is_best and 'state_dict' in states:
        torch.save(states['best_state_dict'],
                   os.path.join(output_dir, 'model_best.pth'))


cfg = model_cfgs.config_moses_dec5_2


hd_train = HeatmapDataset_Train(cfg)  # , "augmentation3/nikitha.csv", False)
# hd_test = MosesHeatmapDataset(cfg, "augmentation2/test.csv", True)

# d = hd[5]


dataloader_train = DataLoader(hd_train, batch_size=1, 
                              shuffle=True, num_workers=0) # cfg.TEST.BATCH_SIZE_PER_GPU*len(cfg.GPUS) = 32*4, maybe more!  WORKERS: 24


model = simdr.get_pose_net(cfg)


learning_rate = 1e-3
batch_size = 64

loss_fn = MosesLoss()


'''
TRAIN:
  BATCH_SIZE_PER_GPU: 32
  SHUFFLE: true
  BEGIN_EPOCH: 0
  END_EPOCH: 140
  OPTIMIZER: 'adam'
  LR: 0.001
  LR_FACTOR: 0.1
  LR_STEP:
  - 90
  - 120
  WD: 0.0001
  GAMMA1: 0.99
  GAMMA2: 0.0
  MOMENTUM: 0.9
  NESTEROV: false
'''

mini_epoch_size = 200
mul = len(dataloader_train)/mini_epoch_size


optimizer = torch.optim.Adam(model.parameters())  # , momentum=0.9, weight_decay=0.0001, nesterov=False) # , lr=0.001

# model = torch.nn.DistributedDataParallel(model, device_ids = (0))
model = torch.nn.DataParallel(model, device_ids = (0))

# checkpoint_file = os.path.join(
#     "checkpoints", 'december15.pth'
# )

# checkpoint = torch.load(checkpoint_file)
# begin_epoch = checkpoint['epoch']
# mini_epoch = checkpoint['mini_epoch']
# best_perf = checkpoint['perf']
# last_epoch_for_lr_scheduler = checkpoint['mini_epoch']
# model.load_state_dict(checkpoint['state_dict'])

# inp = torch.randn(, 3, 224, 224)

# bob = model(inp)
# print(bob)

# exit(0)

checkpoint_file = os.path.join(
    "checkpoints", 'checkpoint.pth'
)
last_epoch_for_lr_scheduler = -1  # importante!
begin_epoch = 0
mini_epoch = 0

if os.path.exists(checkpoint_file):
    checkpoint = torch.load(checkpoint_file)
    begin_epoch = checkpoint['epoch']
    mini_epoch = checkpoint['mini_epoch']
    best_perf = checkpoint['perf']
    last_epoch_for_lr_scheduler = checkpoint['mini_epoch']
    model.load_state_dict(checkpoint['state_dict'])

    optimizer.load_state_dict(checkpoint['optimizer'])

# we want lr_step to be 170, 200 and end_epoch to be 210: those milestones come "before"

print("Scheduler resuming at", last_epoch_for_lr_scheduler)
# Mini-epochs. 100 mini-epochs per epoch.
lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(
    optimizer, (90*mul, 120*mul), 0.1,
    last_epoch=last_epoch_for_lr_scheduler
)


for epoch in range(begin_epoch, 200):
    print(f"Epoch {epoch+1}\n---------------------------------------")
    mini_epoch = train_loop(mini_epoch_size, mini_epoch, epoch, lr_scheduler, dataloader_train, model, loss_fn, True, optimizer)

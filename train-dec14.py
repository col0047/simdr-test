import argparse
import os
import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt
from torch.utils import data
from torch.utils.data import Dataset, DataLoader
from opencv_transforms import transforms

import heatmap_1d
import faster_simdr as simdr
import model_cfgs

import scipy.optimize
import matplotlib.pyplot as plt
import random

import sys
import select

# https://gitanswer.com/pytorch-too-many-open-files-error-cplusplus-356516297
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

parser = argparse.ArgumentParser(description='Train keypoints network')
parser.add_argument('--no-gui',
                    help="Don't run the little debug gui",
                    dest='gui', action='store_false'
                    )
parser.add_argument('--gpu',
                    help="Use GPU",
                    dest='gpu', action='store_true'
                    )
parser.set_defaults(gui=True)
parser.set_defaults(gpu=False)
args = parser.parse_args()

print(args.gui)
print(args.gpu)

# Subsequent GPU/CPU stuff is cursed but I will not make it perfect, sorry.
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# CPU defaults
num_devices = 1
batch_size_per_device = 16

# if device == "cuda:0":
if (args.gpu):
    num_devices = torch.cuda.device_count()
    print(f"Let's use {num_devices} GPUs!")
    # Warning, this can OOM your RAM if too high. At least right now. Be careful :)
    batch_size_per_device = 256
    batch_size_per_device = 512


class HeatmapDataset_Train(Dataset):

    base_path = "augmentation5"

    data_sources = [
        {"filename": f"{base_path}/nikitha.csv", "proportion": 0.5},
        {"filename": f"{base_path}/frei_gs.csv", "proportion": 0.5},
        {"filename": f"{base_path}/mt_oct27.csv", "proportion": 0.3},
        {"filename": f"{base_path}/tom_17.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_23.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_24.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_26.csv", "proportion": 0.2},
        {"filename": f"{base_path}/tom_27.csv", "proportion": 0.2},
        {"filename": f"{base_path}/pano_human.csv", "proportion": 0.6},
        {"filename": f"{base_path}/pano_synth.csv", "proportion": 0.8},
        {"filename": f"{base_path}/pano_pano.csv", "proportion": 1.0},
    ]

    def __init__(self, cfg):
        assert cfg["MODEL"]["IMAGE_SIZE"][0] == cfg["MODEL"]["IMAGE_SIZE"][1], "You can turn this off if you want, but currently a lot of code assumes square input size"
        self.input_size = cfg["MODEL"]["IMAGE_SIZE"][0]
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # This is what ImageNet uses?

        sum = 0.0

        for thing in self.data_sources:
            thing["csv_frame"] = pd.read_csv(thing["filename"], delimiter=" ", quotechar="|")
            thing["length"] = len(thing["csv_frame"])
            sum += thing["proportion"]
        for thing in self.data_sources:
            thing["proportion"] *= (1.0/sum)
            print(thing["proportion"])

    def __len__(self):
        # c: int = 0
        # for thing in self.data_sources:
        #     c += len(thing["csv_frame"])
        # return c

        # Todo: This is accurate on December 16.
        # I need to simplify this to fix the input source distribution so that we don't do random sources from here because that's a mess.
        # return 10000
        # return 100000
        return 587970

    def __getitem__(self, idx):

        dataset_choice_a = random.uniform(0, 1)
        curr = 0
        thing = None
        for thinga in self.data_sources:
            curr += thinga["proportion"]
            if dataset_choice_a < curr:
                thing = thinga
                break
        if thing == None:
            raise RuntimeError


        # if dataset_choice_a > 0.5:
        #     thing = self.data_sources[1]

        idx = random.randint(0, thing["length"]-1)

        row = thing["csv_frame"].iloc[idx]

        # Input
        img_name = os.path.join(self.base_path,
                                row[0])  # thing["csv_frame"].iloc[idx, 0])
        image_numpy = cv2.imread(img_name)

        image_numpy = image_numpy.astype(np.float32)*1.0/255.0

        simDR_axis_size = self.input_size

        image_numpy = cv2.resize(image_numpy, (simDR_axis_size, simDR_axis_size))

        # cv2.imshow("input", image_numpy)

        for_normalize = np.zeros((3, simDR_axis_size, simDR_axis_size), dtype=np.float32)

        # Convert from interleaved to planar as well as BGR to RGB
        for_normalize[0] = image_numpy[:, :, 2]
        for_normalize[1] = image_numpy[:, :, 1]
        for_normalize[2] = image_numpy[:, :, 0]

        input_as_tensor = torch.from_numpy(for_normalize)

        input_as_tensor = self.normalize(input_as_tensor)

        # Output
        # outputX, outputY should be 1, 21, 512 (NOT 1x1x21x512, 1x21x512).
        output_x_axis = np.zeros((21, simDR_axis_size))
        output_y_axis = np.zeros((21, simDR_axis_size))

        # blorg = np.zeros((512, 512))

        jointlocs = np.zeros((21, 2))

        for i in range(21):
            # y = self.landmarks_frame.iloc[idx, i*2+2] * simDR_axis_size/224
            # x = self.landmarks_frame.iloc[idx, i*2+1] * simDR_axis_size/224
            y = row[i*2+2] * simDR_axis_size/224
            x = row[i*2+1] * simDR_axis_size/224
            output_x_axis[i] = heatmap_1d.heatmap_1d(simDR_axis_size, x, simDR_axis_size*0.04)
            output_y_axis[i] = heatmap_1d.heatmap_1d(simDR_axis_size, y, simDR_axis_size*0.04)
            jointlocs[i][0] = x
            jointlocs[i][1] = y
            # blorg += heatmap_1d.two_heatmaps_to_2d(output_x_axis[0][i], output_y_axis[0][i])

        # cv2.imshow("blorg", blorg)
        # cv2.imshow("x", output_x_axis[0])
        # cv2.imshow("y", output_y_axis[0].T)

        # cv2.waitKey(0)

        sample = {
            'input_image': input_as_tensor,
            'gt_x': torch.from_numpy(output_x_axis).float(),
            'gt_y': torch.from_numpy(output_y_axis).float(),
            'disp_image': torch.from_numpy(image_numpy), "gt_joint_locs": torch.from_numpy(jointlocs)}
        # print("from source:");
        # for k in sample.keys():
        #     print(k, sample[k].shape)
        # print("\n")

        return sample


class MosesLoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.float()
        self.criterion = nn.MSELoss()

    def forward(self, pred_x, pred_y, gt_x, gt_y):
        loss = 0
        loss += self.criterion(pred_x, gt_x).sum()
        loss += self.criterion(pred_y, gt_y).sum()
        return loss


def check_for_input():
    while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = sys.stdin.readline()
        if line:
            if (line == "\n"):
                print("exiting!");
                # Safe to exit! Only unsafe time is when we are actually saving the checkpoint
                exit(0) 
        else: # an empty line means stdin has been closed
            print('eof')
            exit(0)



def gaus(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))


def display_output(input_img, gt_x, gt_y, output_x, output_y, gt_jts):

    # output_grid = np.zeros((224, 224, 3))
    inp_to_gauss = np.arange(224)

    jointlocs_gauss = np.zeros((21, 2))
    jointlocs_argmax = np.zeros((21, 2))

    for i in range(21):
        center_x = np.argmax(output_x[i])
        jointlocs_argmax[i][0] = center_x

        try:
            popt, pcov = scipy.optimize.curve_fit(gaus, inp_to_gauss, output_x[i], p0=[.87, center_x, 13])
            jointlocs_gauss[i][0] = popt[1]
        except RuntimeError:
            print("Couldn't fit X!")
            pass

        center_y = np.argmax(output_y[i])
        jointlocs_argmax[i][1] = center_y

        try:
            popt, pcov = scipy.optimize.curve_fit(gaus, inp_to_gauss, output_y[i], p0=[.87, center_y, 13])
            jointlocs_gauss[i][1] = popt[1]
        except RuntimeError:
            print("Couldn't fit Y!")
            pass

    def draw_lines(thing, color, width=2):
        for finger in range(5):
            for joint in range(4):
                idx = 1 + (finger*4) + joint

                previdx = idx-1
                if joint == 0:
                    previdx = 0

                prev_gt = thing[previdx]
                curr_gt = thing[idx]

                cv2.line(input_img, (int(prev_gt[0]), int(prev_gt[1])), (int(curr_gt[0]), int(curr_gt[1])), color, width)

    draw_lines(gt_jts, (0, 255, 0))
    # draw_lines(jointlocs_argmax, (0, 128, 200))
    draw_lines(jointlocs_gauss, (0, 0, 255))

    # cv2.circle(output_grid, (int(cX), int(cY)), 7, (255, 255, 255), cv2.FILLED)
    # paramsX, covX = scipy.optimize.curve_fit(Gauss, inp_to_gauss, output_x[i])
    # paramsY, covY = scipy.optimize.curve_fit(Gauss, inp_to_gauss, output_y[i])

    # print(paramsX, paramsY)

    # xloc = paramsX[0]
    # yloc = paramsY[0]
    if args.gui:
        cv2.imshow("ground-truth output:", heatmap_1d.two_heatmaps_to_2d(gt_x[12], gt_y[12]))
        cv2.imshow("model output x:", output_x)
        cv2.imshow("model output y:", output_y.T)
        cv2.imshow("model output:", heatmap_1d.two_heatmaps_to_2d(output_x[12], output_y[12]))
        cv2.imshow("preedicted joints", input_img)

        # cv2.imshow("test! output", np.hstack((output.detach().numpy()[0][0], pred.detach().numpy()[0][0])))
        key = cv2.waitKey(1)
        # Exiting cleanly is important; sometimes you can accidentally ctrl+c when saving the state dict
        if (key == ord('q')):
            # if (train):
            #     print("Saving!")
            #     torch.save(model.state_dict(), model.state_path)
            exit(0)


def train_loop(epoch, scheduler, dataloader, model, loss_fn, train=True, optimizer=None):
    total_loss = 0
    loss_divisor = 0
    l = len(dataloader)
    for batch, doct in enumerate(dataloader):
        print(f"{batch}/{l}")
        input = doct["input_image"].to(device)
        gt_x = doct["gt_x"].to(device)
        gt_y = doct["gt_y"].to(device)

        pred = model(input)
        loss = loss_fn(pred[0], pred[1], gt_x, gt_y)
        
        total_loss += loss
        loss_divisor += 1

        if (train):
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        if batch % 50 == 0:
            # loss, current = loss.item(), batch*len(X)
            # torch.save(model.state_dict(), model.state_path)
            print(f"loss: {loss:>7f}")

            check_for_input()
            
            # print("PARAMETERS:", sum(p.numel() for p in model.parameters() if p.requires_grad))

            # cv2.imshow("ground-truth output:", heatmap_1d.two_heatmaps_to_2d(gt_x.detach().numpy()[0][0], gt_y.detach().numpy()[0][0]))
            # cv2.imshow("model output:", heatmap_1d.two_heatmaps_to_2d(pred[0].detach().numpy()[0][0], pred[1].detach().numpy()[0][0]))
            # cv2.imshow("model output x:", pred[0].detach().numpy()[0])
            # cv2.imshow("model output y:", pred[1].detach().numpy()[0].T)

            # Get first element in batch because reasons
            if args.gui:
                disp_img = doct["disp_image"].detach().to('cpu').numpy()[0]
                gt_x = doct["gt_x"].detach().to('cpu').numpy()[0]
                gt_y = doct["gt_y"].detach().to('cpu').numpy()[0]
                pred_x = pred[0].detach().to('cpu').numpy()[0]
                pred_y = pred[1].detach().to('cpu').numpy()[0]
                gt_jts = doct["gt_joint_locs"].detach().to('cpu').numpy()[0]

                display_output(disp_img, gt_x, gt_y, pred_x, pred_y, gt_jts)
    print(f"Avg loss this batch: {total_loss/loss_divisor}")
    return total_loss


def save_checkpoint(states, output_dir, filename='checkpoint.pth'):
    torch.save(states, os.path.join(output_dir, filename))
    # if is_best and 'state_dict' in states:
    #     torch.save(states['best_state_dict'],
    #                os.path.join(output_dir, 'model_best.pth'))


cfg = model_cfgs.config_moses_dec5_2


hd_train = HeatmapDataset_Train(cfg)


dataloader_train = DataLoader(hd_train, batch_size=batch_size_per_device*num_devices,
                              shuffle=True, num_workers=torch.multiprocessing.cpu_count())


model = simdr.get_pose_net(cfg)
model = torch.nn.DataParallel(model).to(device)


loss_fn = MosesLoss()


'''
TRAIN:
  BATCH_SIZE_PER_GPU: 32
  SHUFFLE: true
  BEGIN_EPOCH: 0
  END_EPOCH: 140
  OPTIMIZER: 'adam'
  LR: 0.001
  LR_FACTOR: 0.1
  LR_STEP:
  - 90
  - 120
  WD: 0.0001
  GAMMA1: 0.99
  GAMMA2: 0.0
  MOMENTUM: 0.9
  NESTEROV: false
'''

optimizer = torch.optim.Adam(model.parameters())  # , momentum=0.9, weight_decay=0.0001, nesterov=False) # , lr=0.001

checkpoint_file = os.path.join(
    "checkpoints", 'checkpoint.pth'
)

begin_epoch = 0

lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(
    optimizer, (90, 120), 0.1,
)

best_performance = 100000000000000

if os.path.exists(checkpoint_file):
    checkpoint = torch.load(checkpoint_file, map_location=torch.device(device))
    begin_epoch = checkpoint['epoch']
    best_performance = checkpoint['best_performance']
    model.load_state_dict(checkpoint['state_dict'])
    lr_scheduler.load_state_dict(checkpoint['scheduler'])
    optimizer.load_state_dict(checkpoint['optimizer'])



for epoch in range(begin_epoch, 200):
    print(f"Epoch {epoch}\n---------------------------------------")
    total_loss = train_loop(epoch, lr_scheduler, dataloader_train, model, loss_fn, True, optimizer)
    final_output_dir = "checkpoints"
    best_model = False
    if total_loss < best_performance:
        best_model = True
        best_performance = total_loss

    save_checkpoint({
        'epoch': epoch,
        'model': "i dont know what to call this",
        'state_dict': model.state_dict(),
        'best_performance': best_performance,  # float
        'optimizer': optimizer.state_dict(),
        'scheduler': lr_scheduler.state_dict(),
    }, final_output_dir)
    lr_scheduler.step()
    if epoch % 10 == 0:
        print(f"Epoch {epoch}, saving extra!")
        os.system(f"cp checkpoints/checkpoint.pth checkpoints/checkpoint_{epoch}.pth")
    if best_model:
        print("Best! Saving as such!")
        os.system(f"cp checkpoints/checkpoint.pth checkpoints/checkpoint_best.pth")
